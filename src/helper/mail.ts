import nodemailer, { type Transporter } from "nodemailer";
import type SMTPTransport from "nodemailer/lib/smtp-transport";
import { env } from "./env";

let transporter: Transporter<SMTPTransport.SentMessageInfo>;
let initRan = false;

const textMark = "[ galaxy [BETA] ] | https://galaxy.click\n\n";
const htmlMark = `
	<div>
		<a class="galaxy-signature" href="https://galaxy.click" aria-label="Galaxy Beta"
			style="background: black; padding: 1em; display: inline-flex; text-decoration: none">
			<span style="color: white; font-family: monospace;">galaxy</span>
			<span style="background: white; color: black; margin-inline-start: .5em; padding: .25em; font-size: .8em" class="beta-tag">BETA</span>
		</a>
	</div>
`;

async function main() {
	initRan = true;

	let user = "";
	let pass = "";

	const mode = env.MAIL_TYPE;
	if (mode === "ethereal") {
		const test = await nodemailer.createTestAccount();
		user = test.user;
		pass = test.pass;
	} else {
		user = env.MAIL_USER;
		pass = env.MAIL_PASS;
	}

	transporter = nodemailer.createTransport({
		host: mode === "prod" ? env.MAIL_HOST : "smtp.ethereal.email",
		port: mode === "prod" ? env.MAIL_PORT : 587,
		secure: false,
		auth: { user, pass },
		...(env.DKIM_ENABLED
			? {
					dkim: {
						domainName: env.DKIM_DOMAIN_NAME,
						keySelector: env.DKIM_KEY_SELECTOR,
						privateKey: env.DKIM_PRIVATE_KEY,
					},
				}
			: {}),
	});
}

export async function sendMail(
	to: string,
	subject: string,
	body: { plaintext: string; html: string }
) {
	if (!initRan) await main();
	const info = await transporter.sendMail({
		from: "galaxy.click <noreply@galaxy.click>",
		to,
		subject,
		text: textMark + body.plaintext,
		html: htmlMark + body.html,
	});

	if (env.MAIL_TYPE === "ethereal")
		console.log(nodemailer.getTestMessageUrl(info));
}

export async function sendVerifyMail(email: string, code: string) {
	await sendMail(email, "verify your email on galaxy.click", {
		plaintext: `
-=<[ welcome to galaxy ]>=-

Thanks for checking out galaxy.click! 

Your verification code is: ${code}
Please copy this verification code and paste it into the verification box to verify your account.

If you didn't perform this action, you can safely ignore this email.
		`.trim(),
		html: `
			<h1>welcome to galaxy</h1>
			<p>Thanks for checking out <b>galaxy.click</b>!</p>
			<p>
				Click <a href="https://galaxy.click/signedup?code=${code}">this link</a>
				to verify your account, or copy this verification code and paste it into the verification box:
				<code>${code}</code>
			</p>
			<p>If you didn't perform this action, you can safely ignore this email.</p>
		`.trim(),
	});
}
