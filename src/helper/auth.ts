import type { AstroCookies } from "astro";
import type { HydratedDocument } from "mongoose";
import type { IUser } from "~/types";
import type { JwtPayload } from "jsonwebtoken";
import { User } from "~/models/user";
import { UserAuth } from "~/models/userauth";
import { env } from "./env";
import jwt from "jsonwebtoken";
import { userUnder13 } from "./coppa";

// user must be present if "logged in" or requiring verification, but may not be otherwise
export type AuthRequest =
	| {
			user: HydratedDocument<IUser>;
			loggedIn: true;
			needsVerification: false;
			isUnderage: boolean;
			shouldLogout: boolean;
	  }
	| {
			user: HydratedDocument<IUser>;
			loggedIn: false;
			needsVerification: true;
			isUnderage: boolean;
			shouldLogout: boolean;
	  }
	| {
			user?: HydratedDocument<IUser>;
			loggedIn: false;
			needsVerification: false;
			isUnderage: boolean;
			shouldLogout: boolean;
	  };

export async function tokenToUser(token?: string): Promise<AuthRequest> {
	if (token !== undefined) {
		try {
			const decoded = jwt.verify(token, env.AUTH_SECRET) as JwtPayload;

			const [user, userAuth] = await Promise.all([
				User.findOne({ id: decoded.id }),
				UserAuth.findOne({ id: decoded.id }),
			]);
			if (user !== null) {
				const emailVerified = userAuth.emailVerified === true;

				if (userAuth.sessionState !== decoded.state)
					return {
						loggedIn: false,
						shouldLogout: true,
						needsVerification: false,
						isUnderage: false,
					};
				if (emailVerified !== true)
					return {
						loggedIn: false,
						shouldLogout: false,
						needsVerification: true,
						isUnderage: userUnder13(userAuth),
						user,
					};
				if (user.banned !== false)
					return {
						loggedIn: false,
						shouldLogout: false,
						needsVerification: false,
						isUnderage: userUnder13(userAuth),
						user,
					};
				if (userAuth.parentState === 0)
					return {
						loggedIn: false,
						shouldLogout: false,
						needsVerification: true,
						isUnderage: userUnder13(userAuth),
						user,
					};
				return {
					loggedIn: true,
					shouldLogout: false,
					needsVerification: false,
					isUnderage: userUnder13(userAuth),
					user,
				};
			} else {
				return {
					loggedIn: false,
					shouldLogout: true,
					needsVerification: false,
					isUnderage: false,
				};
			}
		} catch {}
	}

	return { loggedIn: false, shouldLogout: false, needsVerification: false, isUnderage: false };
}

export function astroCookiesToUser(
	cookies: AstroCookies
): Promise<AuthRequest> {
	return tokenToUser(cookies.get("token")?.value);
}

// You can't check sessionState shtuff without querying the user,
// hence why this function is removed
// export function tokenToId(token: string): {
// 	id: number;
// 	loggedIn: boolean;
// } {
// 	if (token) {
// 		const decoded = jwt.verify(
// 			token,
// 			config.get("secret")
// 		);

// 		return { id: decoded.id, loggedIn: true };
// 	}

// 	return { id: -1, loggedIn: false };
// }
