import type { HydratedDocument } from "mongoose";
import type { IUser } from "~/types";
import { User } from "~/models/user";

export async function findUsers(users: number[], keepSensitive = false) {
	// Remove duplicates
	// TODO i could probably do some one-liner with indexOf and filter
	const userSet: Set<number> = new Set();
	users.forEach(user => userSet.add(user));
	users = [...userSet.keys()];

	const out: { [id: number]: HydratedDocument<IUser> } = {};

	const foundUsers = await User.find({ id: { $in: users } });

	foundUsers.forEach(u => {
		if (u) out[u.id] = keepSensitive ? u : u.toJSON();
	});

	return out;
}
