import { GameSave } from "~/models/gamesave";
import { rdata } from "~/helper/res";
import { route } from "~/helper/route";
import { z } from "zod";

const schema = z.object({
	game: z.number().int(),
});

export const POST = route(
	{
		auth: true,
		schema,
	},
	async ({ locals }, req) => {
		const { user } = locals.auth;

		const saves = await GameSave.find({
			game: req.game,
			user: user.id,
		});

		const frontendSaves = saves
			.sort((a, b) => a.slot - b.slot)
			.map(save => ({
				game: req.game,
				user: user.id,
				slot: save.slot,
				data: save.data,
				label: save.label,
				time: +save.updatedAt,
			}));

		return rdata({
			message: "Load successful",
			game: req.game,
			user: user.id,
			saves: frontendSaves,
		});
	}
);
