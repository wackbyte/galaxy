import { getAllTags } from "~/helper/tags";
import { rdata } from "~/helper/res";

export async function GET() {
	return rdata(getAllTags(), 200);
}
