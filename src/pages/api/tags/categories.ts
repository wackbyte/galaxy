import { getTagCategories } from "~/helper/tags";
import { rdata } from "~/helper/res";

export async function GET() {
	return rdata(getTagCategories(), 200);
}
