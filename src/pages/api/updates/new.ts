import type { IGame, IUpdate } from "~/types";
import { Update, updateSchema } from "~/models/update";
import { byLockdown, route } from "~/helper/route";
import { rfu, rmsg } from "~/helper/res";
import { Favorite } from "~/models/favorite";
import { Game } from "~/models/game";
import { Message } from "~/models/message";
import { convertFormat } from "~/libgalaxy";
import { env } from "~/helper/env";
import { keyv } from "~/models/keyv";
import { sendFancyWebhook } from "~/helper/sendWebhook";

export const POST = route(
	{
		disabled: byLockdown(
			"updatesLocked",
			"The creation of new updates is disabled at this moment."
		),
		auth: true,
		notMuted: true,
		schema: updateSchema,
	},
	async ({ locals }, req) => {
		const { user } = locals.auth;

		// Find the game
		const gameId = req.game;
		const game = await Game.findOne({ id: gameId });
		if (!game) return rmsg("Attempted to update unknown game", 400);

		const author = user.id;
		if (author !== game.author)
			return rfu("You didn't make this game", 403);

		const lastUpdateId = await keyv.id("lastUpdateId");

		const update = new Update({
			game: gameId,
			changelog: req.changelog,
			version: req.version,
			name: req.name,
			id: lastUpdateId,
		});

		if (!req.silent) {
			if (env.UPDATE_WEBHOOK !== undefined) {
				const updateString =
					req.name !== undefined
						? req.version !== undefined
							? `**${req.name}** - ${req.version}`
							: `**${req.name}**`
						: req.version !== undefined
							? `**${req.version}**`
							: "[no update name]";

				// this is what i get for not putting 3 more hours into the depths
				// of markdown-it before i made !! spoiler syntax instead of ||
				const updateDesc = req.changelog.replaceAll("!!", "||");

				await sendFancyWebhook(env.UPDATE_WEBHOOK, {
					username: user.name,
					avatar_url: convertFormat(
						"https://galaxy.click/pfp/large/",
						user.id,
						user.pfpTimestamp
					),
					embeds: [
						{
							title: game.name,
							description: `${updateString}\n\n${updateDesc}`,
							url: `https://galaxy.click/play/${game.id}`,
							color: 4723016,
							footer: {
								text: "new update published",
							},
							timestamp: new Date().toISOString(),
							image: {
								url: convertFormat(
									"https://galaxy.click/thumb/large/",
									game.id,
									game.thumbTimestamp
								),
							},
						},
					],
				});
			}

			game.lastUpdate = Date.now();
			await game.save();
		}
		await update.save();

		if (!req.silent) sendUpdateNotices(req as IUpdate, game);

		return rmsg(
			req.silent
				? "Update published!"
				: "Update published! Enjoy the bump.",
			201
		);
	}
);

async function sendUpdateNotices(req: IUpdate, game: IGame) {
	const faves = await Favorite.find({ game: req.game });
	let content = "";
	if (
		req.name != null &&
		req.name !== "" &&
		req.version != null &&
		req.version !== ""
	)
		content = `${req.name} - ${req.version}`;
	else if (req.name != null && req.name !== "") content = req.name;
	else if (req.version != null && req.version !== "") content = req.version;

	if (content !== "") content += "\n\n";
	content += req.changelog;

	content += `\n\n[play it here](/play/${game.id})`;

	faves.forEach(async fave => {
		const msg = new Message({
			from: -1,
			to: fave.user,
			title: `${game.name} was updated`,
			content,
		});

		await msg.save();
	});
}
