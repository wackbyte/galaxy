import { ForumPost } from "~/models/forumpost";
import { adminDataRoute } from "~/helper/route";
import { z } from "zod";

const schema = z.object({
	page: z.number().int().min(1).default(1),
	user: z.number().int().optional(),
	thread: z.number().int().optional(),
});

export const POST = adminDataRoute(
	{
		schema,
	},
	async (_request, data) => {
		const query = {
			...(data.user !== undefined ? { author: data.user } : {}),
			...(data.thread !== undefined ? { thread: data.thread } : {}),
		};

		const pageCount = Math.ceil(
			(await ForumPost.countDocuments(query)) / 100
		);
		const result = (
			await ForumPost.find(query)
				.sort("-id")
				.limit(100)
				.skip(100 * (data.page - 1))
		).map(x => x.toObject());

		return { result, pageCount };
	}
);
