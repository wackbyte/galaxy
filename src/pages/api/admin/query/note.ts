import { StaffNote } from "~/models/staffnote";
import { adminDataRoute } from "~/helper/route";
import { z } from "zod";

export const POST = adminDataRoute(
	{
		reqLevel: "verifier",
		schema: z.string(),
	},
	async (_request, noteId) => {
		const note = await StaffNote.findOne({ id: noteId });
		if (!note) return { data: null };

		return { data: note };
	}
);
