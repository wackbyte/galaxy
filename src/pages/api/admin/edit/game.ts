import { Game } from "~/models/game";
import { adminRoute } from "~/helper/route";
import { logAction } from "~/helper/adminHelper";
import { rmsg } from "~/helper/res";
import { z } from "zod";

const schema = z.object({
	id: z.number().int(),
	name: z.string().optional(),
	link: z.string().optional(),
	description: z.string().optional(),
	unlisted: z.boolean().optional(),
	tags: z.array(z.string()).optional(),
	author: z.number().int().optional(),
	verified: z.boolean().or(z.string()).optional(),
});

export const POST = adminRoute(
	{ reqLevel: "verifier", schema },
	async (_request, editing, mod) => {
		const g = await Game.findOne({ id: editing.id });
		if (!g) return rmsg("Game not found", 404);

		let editedSummary = "";

		for (const prop in editing) {
			// I can't just do a raw !== here because tags are an array :pensive:
			if (JSON.stringify(g[prop]) !== JSON.stringify(editing[prop])) {
				editedSummary += `${prop}: ${g[prop]} -> ${editing[prop]}`;
				g[prop] = editing[prop];
			}
		}

		await g.save();

		if (editedSummary !== "")
			await logAction(
				mod.id,
				"game",
				`updated info for game ${g.name} (${g.id})`,
				editedSummary.trim(),
				{ editing }
			);
	}
);
