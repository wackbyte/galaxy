import { adminRoute } from "~/helper/route";
import { keyv } from "~/models/keyv";
import { logAction } from "~/helper/adminHelper";
import { z } from "zod";

const schema = z.object({
	comments: z.boolean(),
	games: z.boolean(),
	updates: z.boolean(),
	users: z.boolean(),
	feedback: z.boolean(),
	messages: z.boolean(),
	chat: z.boolean(),
	forum: z.boolean(),
});

export const POST = adminRoute(
	{ schema },
	async (_request, data, mod) => {
		await Promise.all(
			Object.entries(data).map(([key, value]) =>
				keyv.set(`${key}Locked`, value)
			)
		);

		await logAction(
			mod.id,
			"lockdown",
			`changed lockdown state`,
			Object.entries(data)
				.map(parts => parts.join(": "))
				.join(", "),
			{ data }
		);
	}
);
