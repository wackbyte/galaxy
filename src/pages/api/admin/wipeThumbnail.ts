import { Game } from "~/models/game";
import { adminRoute } from "~/helper/route";
import { rmsg } from "~/helper/res";
import sharp from "sharp";
import { updateThumbs } from "~/helper/img";
import { z } from "zod";

const schema = z.object({
	game: z.number().int(),
});

export const POST = adminRoute(
	{ reqLevel: "verifier", schema },
	async (_request, { game }) => {
		const g = await Game.findOne({ id: game });
		if (!g) return rmsg("Game not found", 404);

		const original = g.thumbTimestamp;
		g.thumbTimestamp = Date.now();
		await g.save();

		await updateThumbs(
			sharp("public/unknown-game.webp"),
			game,
			g.thumbTimestamp,
			original
		);
	}
);
