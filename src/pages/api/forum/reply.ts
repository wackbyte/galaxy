import { byLockdown, route } from "~/helper/route";
import { rdata, rmsg } from "~/helper/res";
import { ForumCategory } from "~/models/forumcategory";
import { ForumPost } from "~/models/forumpost";
import { ForumThread } from "~/models/forumthread";
import { keyv } from "~/models/keyv";
import { z } from "zod";

const schema = z.object({
	body: z
		.string()
		.trim()
		.max(16384, "Post body can not exceed 16,384 characters"),
	thread: z.number().int(),
});

export const POST = route(
	{
		auth: true,
		notMuted: true,
		notUnderage: true,
		schema,
		disabled: byLockdown(
			"forumLocked",
			"The forums are disabled at this moment."
		),
	},
	async ({ locals }, req) => {
		const { user } = locals.auth;
		const { body, thread } = req;

		if (user.playMinutes < 5)
			return rmsg(
				"Your account is too new to make a forum post! Try playing some games first.",
				400
			);

		const t = await ForumThread.findOne({
			id: thread,
			deleted: false,
		});
		if (!t) return rmsg("Thread not found", 400);
		if (t.locked !== false) return rmsg("This thread is locked.", 400);

		const cat = await ForumCategory.findOne({ id: t.category });
		if (!cat) return rmsg("Category not found", 400);
		// TODO allow posting in locked threads if moderator
		// if (cat.locked !== false)
		// 	return rmsg("This category is locked at the moment, try again later.", 400);

		const timestamp = new Date();
		const postId = await keyv.id("lastPostId");
		const post = new ForumPost({
			author: user.id,
			body,
			id: postId,
			when: timestamp,
			thread,
		});

		await Promise.all([
			post.save(),
			ForumThread.updateOne(
				{ id: thread },
				{ lastPost: timestamp, $inc: { postCount: 1 } }
			),
			ForumCategory.updateOne(
				{ id: t.category },
				{ lastPost: timestamp, $inc: { postCount: 1 } }
			),
		]);

		return rdata({ message: "ok" });
	}
);
