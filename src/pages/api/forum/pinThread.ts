import { ForumCategory } from "~/models/forumcategory";
import { ForumThread } from "~/models/forumthread";
import { forumModeratorLevel } from "~/helper/forum";
import { rmsg } from "~/helper/res";
import { route } from "~/helper/route";
import { z } from "zod";

const schema = z.number().int();

export const POST = route(
	{ auth: true, notMuted: true, schema },
	async ({ locals }, threadId) => {
		const { user } = locals.auth;

		const thread = await ForumThread.findOne({ id: threadId });
		if (!thread) return rmsg("Thread not found", 400);

		const category = await ForumCategory.findOne({ id: thread.category });
		if (!category) return rmsg("Category not found", 400);

		const modLevel = await forumModeratorLevel(user, category);
		if (modLevel === 0) return rmsg("Insufficient permissions", 400);

		await ForumThread.updateOne(
			{ id: threadId },
			{ $set: { pinned: !thread.pinned } }
		);

		return rmsg("ok");
	}
);
