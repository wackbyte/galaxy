import { rdata, rmsg } from "~/helper/res";
import { ForumPost } from "~/models/forumpost";
import { route } from "~/helper/route";
import { z } from "zod";

const schema = z.object({
	// 7 reactions
	emoji: z.number().int().min(0).max(6),
	post: z.number().int(),
});

export const POST = route(
	{ auth: true, notMuted: true, notUnderage: true, schema },
	async ({ locals }, req) => {
		const { user } = locals.auth;
		const { emoji, post } = req;

		const postDoc = await ForumPost.findOne({ id: post });
		if (!postDoc) return rmsg("Post not found", 400);

		if (postDoc.deleted === true)
			return rmsg("You can not react to deleted posts", 400);

		if (typeof postDoc.reactions[emoji] === "undefined")
			return rmsg("Invalid reaction", 400);

		const { reactions } = postDoc;

		if (reactions[emoji].includes(user.id)) {
			// Remove reaction
			await ForumPost.updateOne(
				{ id: post },
				{
					$pullAll: { [`reactions.${emoji}`]: [user.id] },
					$set: {
						[`reactionCounts.${emoji}`]:
							reactions[emoji].length - 1,
					},
				}
			);

			return rdata({
				message: "ok",
				emoji,
				count: reactions[emoji].length - 1,
				enabled: false,
			});
		} else {
			// Add reaction
			await ForumPost.updateOne(
				{ id: post },
				{
					$push: { [`reactions.${emoji}`]: user.id },
					$set: {
						[`reactionCounts.${emoji}`]:
							reactions[emoji].length + 1,
					},
				}
			);

			return rdata({
				message: "ok",
				emoji,
				count: reactions[emoji].length + 1,
				enabled: true,
			});
		}
	}
);
