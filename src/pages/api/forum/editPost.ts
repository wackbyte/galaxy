import { rdata, rmsg } from "~/helper/res";
import { ForumPost } from "~/models/forumpost";
import { ForumThread } from "~/models/forumthread";
import { hasStaffPerm } from "~/helper/adminHelper";
import { route } from "~/helper/route";
import { z } from "zod";

const schema = z.object({
	body: z
		.string()
		.trim()
		.max(16384, "Post body can not exceed 16,384 characters"),
	post: z.number().int(),
});

export const POST = route(
	{ auth: true, notMuted: true, schema },
	async ({ locals }, req) => {
		const { user } = locals.auth;
		const { body, post } = req;

		const p = await ForumPost.findOne({
			id: post,
			deleted: false,
		});
		if (!p) return rmsg("Post not found", 400);

		if (p.author != user.id && !hasStaffPerm(user.modLevel, "mod")) {
			return rmsg(
				"You can't just edit someone else's post like that!!!! :<",
				400
			);
		}

		const thread = await ForumThread.findOne({
			id: p.thread,
		});
		if (thread.locked && !hasStaffPerm(user.modLevel, "mod")) {
			return rmsg(
				"This post can't be edited because the thread containing it is locked.",
				400
			);
		}

		const timestamp = new Date();

		p.edits.push({
			prev: p.body,
			when: timestamp,
			byModerator: p.author != user.id,
		});
		p.body = body;
		p.save();

		return rdata({ message: "ok" });
	}
);
