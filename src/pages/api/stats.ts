import { rdata } from "~/helper/res";
import { getAllTags } from "~/helper/tags";

import { Channel } from "~/models/channel";
import { ChatMessage } from "~/models/chatmessage";
import { Comment } from "~/models/comment";
import { Favorite } from "~/models/favorite";
import { ForumPost } from "~/models/forumpost";
import { ForumThread } from "~/models/forumthread";
import { Game } from "~/models/game";
import { GameSave } from "~/models/gamesave";
import { Message } from "~/models/message";
import { Playtime } from "~/models/playtime";
import { Rating } from "~/models/rating";
import { Update } from "~/models/update";
import { User } from "~/models/user";

export async function GET() {
	const [
        games,
        users,
        activeUsers,
        comments,
        ratings,
        updates,
        faves,
        pms,
        messages,
        channels,
        saves,
        minutes,
        threads,
        posts,
    ] = await Promise.all([
        Game.countDocuments({ verified: true }),
        User.countDocuments({ banned: false }),
        User.countDocuments({
            lastHeartbeat: { $gte: Date.now() - 65000 },
        }),
        Comment.countDocuments({ deleted: false }),
        Rating.estimatedDocumentCount(),
        Update.estimatedDocumentCount(),
        Favorite.estimatedDocumentCount(),
        Message.countDocuments({ deleted: false }),
        ChatMessage.countDocuments({ deleted: false }),
        Channel.estimatedDocumentCount(),
        GameSave.estimatedDocumentCount(),
        Playtime.aggregate([
            {
                $group: {
                    _id: 1,
                    sum: { $sum: "$minutes" },
                },
            },
        ]),
        ForumThread.countDocuments({ deleted: false }),
        ForumPost.countDocuments({ deleted: false }),
    ]);

	return rdata({
        games,
        users,
        activeUsers,
        comments,
        ratings,
        updates,
        faves,
        pms,
        messages,
        channels,
        saves,
        minutes: minutes[0]?.sum ?? 0,
        tags: getAllTags().length,
        threads,
        posts,
    });
}
