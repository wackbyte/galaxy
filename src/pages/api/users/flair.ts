import { User } from "~/models/user";
import { rmsg } from "~/helper/res";
import { route } from "~/helper/route";
import { z } from "zod";

const schema = z.string({
	invalid_type_error: "Invalid flair",
});

export const POST = route(
	{
		auth: true,
		notMuted: true,
		notUnderage: true,
		schema,
	},
	async ({ locals }, req) => {
		const { user } = locals.auth;

		if (!user.flairs.includes(req))
			return rmsg("You don't own that flair", 400);

		await User.updateOne({ id: user.id }, { equippedFlair: req });

		return rmsg("Flair picked", 200);
	}
);
