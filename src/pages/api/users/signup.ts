import { UserAuth, generateTokenHeaders, userSchema } from "~/models/userauth";
import { byLockdown, route } from "~/helper/route";
import { User } from "~/models/user";
import bcrypt from "bcryptjs";
import { env } from "~/helper/env";
import { keyv } from "~/models/keyv";
import { monthUnder13 } from "~/helper/coppa";
import { rmsg } from "~/helper/res";
import { sendVerifyMail } from "~/helper/mail";
import sharp from "sharp";
import { updatePfps } from "~/helper/img";
import { z } from "zod";

const schema = z
	.object({
		captcha: z.string({
			invalid_type_error: "Are you a human?",
		}),
	})
	.and(userSchema);

export const POST = route(
	{
		disabled: byLockdown(
			"usersLocked",
			"The creation of new accounts is disabled this moment."
		),
		schema,
	},
	async ({ request, clientAddress, cookies }, req) => {
		cookies.set("birth-info", req.year + "," + req.month, {
			maxAge: 60 * 60 * 24 * 365 * 2,
			path: "/",
		});

		const formData = new FormData();
		formData.append("secret", env.TURNSTILE_SECRETKEY);
		formData.append("response", req.captcha);
		formData.append(
			"remoteip",
			request.headers.get("CF-Connecting-IP") ?? clientAddress
		);

		const url = "https://challenges.cloudflare.com/turnstile/v0/siteverify";
		const captcha = await fetch(url, {
			body: formData,
			method: "POST",
		});
		const { success } = await captcha.json();
		if (success !== true) return rmsg("Are you a human?", 400);

		// Prevent Gmail symbols or aliases
		// also normalize to lower-case
		const email = req.email
			.split(
				/\.(?=.*@g(?:oogle)?mail.com$)|\+.*(?=@g(?:oogle)?mail.com)/i
			)
			.join("")
			.toLocaleLowerCase();

		// Find an existing user
		// TODO: use User.exists?
		let user = await UserAuth.findOne({ email });
		if (user)
			return rmsg("An account already exists with that email.", 400);
		user = await User.findOne({ name: req.name });
		if (user)
			return rmsg("That username is taken. Try something else.", 400);

		const lastUserId = await keyv.id("lastUserId");

		const newUser = new User({
			name: req.name,
			// password: req.password,
			// email: email,
			id: lastUserId,
		});
		newUser.pfpTimestamp = Date.now();

		const newUserAuth = new UserAuth({
			id: lastUserId,
			email,
			password: await bcrypt.hash(req.password, 12),
			birthYear: req.year,
			birthMonth: req.month,
			...(monthUnder13(req.year, req.month)
				? {
						parentState: 0,
						minorNukeAccount: Date.now() + 1000 * 60 * 60 * 24 * 31,
					}
				: {}),
		});
		if (env.MAIL_TYPE === "none") newUserAuth.emailVerified = true;

		await newUser.save();
		await newUserAuth.save();

		// Because the account was just created, we can safely cast emailVerified to string.
		if (env.MAIL_TYPE !== "none")
			await sendVerifyMail(email, newUserAuth.emailVerified as string);

		// Yes, I know I'm not awaiting this. So what?
		updatePfps(
			sharp("public/unknown-user.webp"),
			lastUserId,
			newUser.pfpTimestamp,
			0
		);

		// TODO i'd switch the cookie code over to this, but i haven't tested if it works yet...
		// cookies.set("token", token, { path: "/", ... })

		return new Response(
			JSON.stringify({
				name: newUser.name,
				email: newUserAuth.email,
				id: newUser.id,
				month: newUserAuth.birthMonth,
				year: newUserAuth.birthYear,
			}),
			{
				status: 201,
				headers: generateTokenHeaders(newUserAuth),
			}
		);
	}
);
