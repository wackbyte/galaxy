import type { APIContext } from "astro";

export async function GET({ cookies, redirect }: APIContext) {
	cookies.delete("token", { path: "/" });

	return redirect("/");
}
