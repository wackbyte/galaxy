import { rdata, rmsg } from "~/helper/res";
import { Message } from "~/models/message";
import { route } from "~/helper/route";

export const GET = route(
	{
		auth: true,
	},
	async ({ locals, url }) => {
		const { user } = locals.auth;

		const id = url.searchParams.get("id");

		const msg = await Message.findOne({ _id: id, deleted: false });
		if (!msg) return rmsg("Message not found", 400);

		if (msg.to !== user.id)
			return rmsg("You are not the recipient of that message", 401);

		msg.read = true;
		await msg.save();

		return rdata({ msg });
	}
);
