import { Message } from "~/models/message";
import { rmsg } from "~/helper/res";
import { route } from "~/helper/route";

export const GET = route(
	{
		auth: true,
	},
	async ({ locals }) => {
		const { user } = locals.auth;

		await Message.updateMany(
			{ to: user.id, read: true, deleted: false },
			{ deleted: true }
		);

		return rmsg("done");
	}
);
