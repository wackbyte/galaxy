import { byLockdown, route } from "~/helper/route";
import { rdata, rmsg } from "~/helper/res";
import { Comment } from "~/models/comment";
import { Game } from "~/models/game";
import { commentSchema } from "~/models/comment";
import { convertFormat } from "~/libgalaxy";
import { env } from "~/helper/env";
import { keyv } from "~/models/keyv";
import { sendFancyWebhook } from "~/helper/sendWebhook";

export const POST = route(
	{
		disabled: byLockdown(
			"commentsLocked",
			"The creation of new comments is disabled at this moment."
		),
		auth: true,
		notMuted: true,
		notUnderage: true,
		schema: commentSchema,
	},
	async ({ locals }, { game: gameId, content }) => {
		const { user } = locals.auth;

		const game = await Game.findOne({ id: gameId });
		if (!game) return rmsg("Attempted to comment on an unknown game", 404);

		if (game.verified !== true)
			return rmsg("Attempted to comment on an unverified game", 400);

		const author = user.id;
		const lastCommentId = await keyv.id("lastCommentId");

		// TODO limit comments per user per game (10?)
		const comment = new Comment({
			game: gameId,
			author,
			content: content,
			id: lastCommentId,
			up: [author],
			upCount: 1,
			down: [],
			score: 1,
		});

		game.comments++;
		await comment.save();
		await game.save();

		if (env.STAFF_COMMENT_WEBHOOK !== undefined)
			await sendFancyWebhook(env.STAFF_COMMENT_WEBHOOK, {
				embeds: [
					{
						title: `new comment on ${game.name}`,
						description: content,
						url: `https://galaxy.click/play/${game.id}`,
						color: 4723016,
						timestamp: new Date().toISOString(),
						author: {
							name: user.name,
							url: `https://galaxy.click/user/${user.id}`,
							icon_url: convertFormat(
								"https://galaxy.click/pfp/large/",
								user.id,
								user.pfpTimestamp
							),
						},
					},
				],
			});

		return rdata(
			{
				message: "success",
				content: {
					author,
					id: comment.id,
					content: content,
					score: 1,
					hidden: false,
					deleted: false,
					createdAt: +comment.createdAt,
					yourVote: 1,

					name: user.name,
					flair: user.equippedFlair,
					pfp: user.pfpTimestamp,
				},
			},
			201
		);
	}
);
