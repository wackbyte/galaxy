import { hasStaffPerm, logAction } from "~/helper/adminHelper";
import { rfu, rmsg } from "~/helper/res";
import { Comment } from "~/models/comment";
import { User } from "~/models/user";
import { route } from "~/helper/route";
import { z } from "zod";

const schema = z.object({
	id: z.number().int(),
});

export const POST = route(
	{ auth: true, schema },
	async ({ locals }, { id: commentId }) => {
		const { user } = locals.auth;

		const comment = await Comment.findOne({
			id: commentId,
			deleted: false,
		});
		if (!comment) return rmsg("Comment does not exist", 404);

		// User must be staff or the author of the comment
		if (!hasStaffPerm(user.modLevel, "mod") && comment.author !== user.id)
			return rfu(
				"You don't have permission to delete this comment!",
				403
			);

		// What is this syntax this scares me
		// `comment.updateOne` looks _very_ similar to `Comment.updateOne`
		await comment.updateOne({
			deleted: true,
		});

		if (hasStaffPerm(user.modLevel, "mod") && comment.author !== user.id) {
			const author = await User.findOne({ id: comment.author });
			await logAction(
				user.id,
				"comment",
				`deleted comment by ${author.name} (${author.id})`,
				`comment content:
${comment.content}

developer reply?:
${comment.devResponse}`,
				{ userId: author.id, comment }
			);
		}

		return rmsg("success", 200);
	}
);
