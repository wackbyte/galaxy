import { Report } from "~/models/report";
import { adminDataRoute } from "~/helper/route";

export const GET = adminDataRoute({}, async () => {
	const reports = await Report.find({ resolved: false });

	return { data: reports };
});
