import { hasNoDuplicates, oneValidTag } from "~/helper/tags";
import { rfu, rmsg } from "~/helper/res";
import { Game } from "~/models/game";
import { route } from "~/helper/route";
import { z } from "zod";

const schema = z.object({
	id: z.number().int(),
	tags: z
		.array(
			z
				.string({
					invalid_type_error: "Invalid tags list",
				})
				.min(2, "Tags shoud be at least 2 characters long")
		)
		.max(30, "Max number of tags is 30"),
});

export const POST = route(
	{
		auth: true,
		notMuted: true,
		schema,
	},
	async ({ locals }, req) => {
		const { user } = locals.auth;

		const game = await Game.findOne({ id: req.id });
		if (!game) return rmsg("Game not found", 400);
		if (game.author !== user.id)
			return rfu("You are not the author of this game", 403);

		if (!hasNoDuplicates(req.tags))
			return rmsg("Duplicate tags are not allowed", 400);

		const invalidTags = req.tags.filter(tag => !oneValidTag(tag));
		if (invalidTags.length)
			return rmsg(`Invlaid tags found: ${invalidTags.join(", ")}`, 400);

		game.tags = req.tags;
		await game.save();

		return rmsg("Tags updated!");
	}
);
