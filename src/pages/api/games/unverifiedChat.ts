import { hasStaffPerm, logAction } from "~/helper/adminHelper";
import { rdata, rfu, rmsg } from "~/helper/res";
import { Game } from "~/models/game";
import { Message } from "~/models/message";
import { UnverifiedGameData } from "~/models/unverifiedgamedata";
import { User } from "~/models/user";
import { env } from "~/helper/env";
import { route } from "~/helper/route";
import sendWebhook from "~/helper/sendWebhook";
import { z } from "zod";
import { sendMail } from "~/helper/mail";
import { UserAuth } from "~/models/userauth";

export const GET = route(
	{ auth: true, notMuted: true },
	async ({ locals, url }) => {
		const id = parseInt(url.searchParams.get("id") ?? "");

		if (isNaN(id)) return rmsg("Game not found", 400);

		const [game, unverifiedData] = await Promise.all([
			Game.findOne({ id }),
			UnverifiedGameData.findOne({ id }),
		]);

		if (game === null) return rmsg("Game not found", 400);
		if (unverifiedData === null) return rdata({ data: [], hasChat: false });
		if (
			game.author !== locals.auth.user.id &&
			!hasStaffPerm(locals.auth.user.modLevel, "verifier")
		)
			return rfu("You do not have permission to view this chat", 403);

		if (!unverifiedData.read.includes(locals.auth.user.id))
			await UnverifiedGameData.updateOne(
				{ id },
				{ $push: { read: locals.auth.user.id } }
			);

		return rdata({ data: unverifiedData.chat, hasChat: true });
	}
);

const schema = z.object({
	game: z.number().int(),
	content: z.string(),
});

export const POST = route(
	{ auth: true, notMuted: true, schema },
	async ({ locals }, { game: id, content }) => {
		const [game, unverifiedData] = await Promise.all([
			Game.findOne({ id }),
			UnverifiedGameData.findOne({ id }),
		]);

		if (game === null || unverifiedData === null)
			return rmsg("Game not found", 400);
		if (
			game.author !== locals.auth.user.id &&
			!hasStaffPerm(locals.auth.user.modLevel, "verifier")
		)
			return rfu("You do not have permission to view this chat", 403);

		const author = locals.auth.user.id === game.author ? "user" : "staff";

		// Don't send a message to the author if they _haven't_ read the latest message.
		// In such a case, they already have a notification to check out the chat.
		if (author === "staff" && (unverifiedData.chat.length === 0 || unverifiedData.read.includes(game.author))) {
			const message = await new Message({
				to: game.author,
				from: -1,
				// when u receive an email from ur bank that's like "we have a new MESSAGE for u log into the portal to see it"
				// so u log in and it's like "hello we are ur bank"
				title: `your game ${game.name} needs attention`,
				content: `
Hello,

Your submission ${game.name} has received a new message from site staff:

${content.replaceAll(/(\n)/g, "$1> ")}

galaxy staff may leave messages to games that has minor problems that need to be fixed before the game can be published.  
To leave a response, visit [the game's edit page](https://galaxy.click/edit/${game.id}) and go to the "staff chat" tab.
				`.trim(),
			});

			await message.save();

			const auth = await UserAuth.findOne({id: game.author});

			await sendMail(auth.email, `your game ${game.name} needs attention`, {
				plaintext: `
-=<[ your game ${game.name} needs attention ]>=-

Your submission ${game.name} has received a new message from site staff:

| ${content.replaceAll("\n", "\n| ")}

galaxy staff may leave messages to games that has minor problems that need to be fixed before the game can be published.  
To leave a response, visit <a href="https://galaxy.click/edit/${game.id}">the game's edit page</a> and go to the "staff chat" tab.
				`.trim(),
				html: `
<h1>your game ${game.name} needs attention</h1>
<p>Your submission <b>${game.name}</b> has received a new message from site staff:</p>
<blockquote>${content.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\n", "<br>")}</blockquote>
<p>
	galaxy staff may leave messages to games that has minor problems that need to be fixed before the game can be published.<br>
	To leave a response, visit the game's edit page and go to the "staff chat" tab: https://galaxy.click/edit/${game.id}
</p>
				`.trim(),
			});
		}

		if (author === "staff") {
			const author = await User.findOne({ id: game.author });
			await logAction(
				locals.auth.user.id,
				"comms",
				`sent a new message in unverified chat ${game.name} (${game.id})`,
				`game posted by ${author.name} (${author.id})
message content:
${content}`,
				{ userId: game.author, game, unverifiedData }
			);
		}

		unverifiedData.chat.push({
			from: author,
			content,
			when: new Date(),
		});
		unverifiedData.read = [locals.auth.user.id];
		await unverifiedData.save();

		if (env.STAFF_GAME_WEBHOOK !== undefined)
			await sendWebhook(env.STAFF_GAME_WEBHOOK, {
				content: `New message in **${game.name}**'s unverified chat by **${author}**:\n${content}`,
			});

		return rdata({ data: unverifiedData.chat, hasChat: true });
	}
);
