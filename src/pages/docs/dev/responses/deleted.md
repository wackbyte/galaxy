---
layout: ~/layouts/Doc.astro
title: galaxy docs - deleted response
---

# `deleted`

The `deleted` response is requested by the [`delete`](/docs/dev/actions/delete) action and contains information about the action's deletion request.

## responding action

- [`delete`](/docs/dev/actions/delete)

## properties

- **`error`** *(boolean)*  
  Whether the action encountered an error.

- **`message`** *(string?)*  
  Present when `error` is true, tells the reason why the action encountered an error. Valid values are:
  + `"no_account"`: The player was logged out.
  + `"invalid_slot"`: The `slot` value was invalid.
  + `"server_error"`: The game couldn't connect to Galaxy's servers.
  
- **`slot`** *(number)*  
  The save slot number.

## examples

If the deletion was successful:

```js
{
	type: "deleted",
	error: false,
	slot: 0,
}
```

If it wasn't:

```js
{
	type: "deleted",
	error: true,
	message: "no_account",
	slot: 0,
}
```