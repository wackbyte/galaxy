---
layout: ~/layouts/Doc.astro
title: galaxy docs - echo
---

# echo property

For any message that galaxy responds to, if the message object contains an `echo` property, the response will have an `echo` property with the same value. The value of `echo` can be anything that can be passed through [the structured clone algorithm](https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API/Structured_clone_algorithm).

This feature is mainly present so complex wrappers around the iframe API can discern between responses when multiple requests are run in parallel.

<note warning>

If `echo` is `null` in a message, the `echo` property will not be present in the response.

</note>

## example usage

Message from game to galaxy:
```js
window.top.postMessage({
	action: "save",
	slot: 0,
	label: "1,000 points",
	data: "{\"points\": 1000}",
	echo: "i can hear myself!"
}, "https://galaxy.click");
```

Response from galaxy to game:
```js
{
	echo: "i can hear myself!"
	type: "saved",
	error: false,
	slot: 0,
}
```