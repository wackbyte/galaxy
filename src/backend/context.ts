import type { CreateWSSContextFnOptions } from "@trpc/server/adapters/ws";
import type { IUser } from "~/types";
import { UserAuth } from "~/models/userauth";
import { tokenToUser } from "~/helper/auth";
import { userUnder13 } from "~/helper/coppa";

const parseCookie = (str: string) => {
	if (!str) return {};
	return str
		.split(";")
		.map(v => v.split("="))
		.reduce(
			(
				acc: {
					[key: string]: string;
				},
				v
			) => {
				acc[decodeURIComponent(v[0].trim())] = decodeURIComponent(
					v[1].trim()
				);
				return acc;
			},
			{}
		);
};

let connCount = 0;
export async function createContext({ req, res }: CreateWSSContextFnOptions) {
	// Create your context based on the request object
	// Will be available as `ctx` in all your resolvers
	// This is just an example of something you might want to do in your ctx fn

	async function getUserFromHeader(): Promise<IUser | null | "UNDERAGE!!!"> {
		if (req.headers.cookie !== undefined) {
			const cookie = parseCookie(req.headers.cookie);
			const user = await tokenToUser(cookie.token);
			if (!user.user) return null;
			if (!user.loggedIn) return null;
			const userAuth = await UserAuth.findOne({ id: user.user.id });
			if (userUnder13(userAuth)) return "UNDERAGE!!!";
			if (user.shouldLogout || user.needsVerification) return null;
			if (user.user.banned !== false) return null;
			if (user.user.muted !== false) return null;
			return user.user;
		}
		return null;
	}

	const user = await getUserFromHeader();
	if (user === "UNDERAGE!!!")
		// By default this sort of just DoS-es the site when an underage user connects to chat--but they shouldn't in the first place, it's restricted on the frontend
		return res.close();
	return {
		user,
		cid: ++connCount,
	};
}

export type Context = Awaited<ReturnType<typeof createContext>>;
