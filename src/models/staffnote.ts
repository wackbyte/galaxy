import { Schema, model } from "mongoose";
import type { IStaffNote } from "~/types";

const StaffNoteSchema = new Schema<IStaffNote>({
	content: String,
	id: String,
	authors: [Number],
}, {
	timestamps: true,
});

export const StaffNote = model<IStaffNote>(
	"staffnote",
	StaffNoteSchema
);
