import { type PaginateModel, Schema, model } from "mongoose";
import type { IForumPost } from "~/types";
import paginate from "mongoose-paginate-v2";

const ForumPostSchema = new Schema<IForumPost>({
	id: Number,
	body: String,
	author: Number,
	thread: Number,
	when: {
		type: Date,
		default: () => new Date(),
	},
	edits: {
		type: [[{ prev: String, when: Date, byModerator: Boolean }]],
		default: [],
	},
	deleted: {
		type: Boolean,
		default: false,
	},
	reactionCounts: {
		type: [Number],
		default: () => new Array(8).fill(0),
	},
	reactions: {
		type: [[Number]],
		default: () => new Array(8).fill([]),
	},
	linkedGame: {
		type: Schema.Types.Mixed,
		default: false,
	},
});

ForumPostSchema.plugin(paginate);

export const ForumPost = model<IForumPost, PaginateModel<IForumPost>>(
	"ForumPosts",
	ForumPostSchema
);
