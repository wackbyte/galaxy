import { Schema, model } from "mongoose";
import type { IUnverifiedGameData } from "~/types";

const UnverifiedGameDataSchema = new Schema<IUnverifiedGameData>(
	{
		id: Number,
		upvotes: [Number],
		downvotes: [Number],
		chat: [
			{
				from: String,
				content: String,
				when: Date,
			},
		],
		read: [Number],
	},
	{
		timestamps: true,
	}
);

export const UnverifiedGameData = model<IUnverifiedGameData>(
	"unverifiedgamedata",
	UnverifiedGameDataSchema
);
