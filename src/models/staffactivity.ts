import { Schema, model } from "mongoose";
import type { IStaffActivity } from "~/types";

const StaffActivitySchema = new Schema<IStaffActivity>(
	{
		staff: Number,
		type: String,
		summary: String,
		fullDescription: String,
		rawData: Schema.Types.Mixed,
	},
	{
		timestamps: true,
	}
);

export const StaffActivity = model<IStaffActivity>("staffactivity", StaffActivitySchema);