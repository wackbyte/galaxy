import { Schema, model } from "mongoose";
import type { IDayGameStats } from "~/types";
import { getCurrentDaynum } from "~/helper/daynums";

const DayGameStatsSchema = new Schema<IDayGameStats>({
	game: Number,
	day: {
		type: Number,
		default: () => getCurrentDaynum(),
	},
	minutes: {
		type: Number,
		default: 0,
	},
	plays: {
		type: Number,
		default: 0,
	},
});

export const DayGameStats = model<IDayGameStats>(
	"DayGameStats",
	DayGameStatsSchema
);
