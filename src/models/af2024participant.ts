import { Schema, model } from "mongoose";
import type { IAF2024Participant } from "~/types";

const AF2024ParticipantSchema = new Schema<IAF2024Participant>({
	clicks: {
		type: Number,
		default: 0,
		index: true,
	},
	first: {
		type: Number,
		default: () => Date.now(),
		index: true,
	},
	last: {
		type: Number,
		default: 0,
		index: true,
	},
	order: Number,
	user: {
		type: Number,
		index: true,
	},
});

export const AF2024Participant = model<IAF2024Participant>(
	"af2024participant",
	AF2024ParticipantSchema
);
