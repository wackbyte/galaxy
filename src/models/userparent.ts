import "~/helper/database-init";

import { Schema, model } from "mongoose";
import type { IUserParent } from "~/types";
import { randomString } from "~/helper/random";

const UserParentSchema = new Schema<IUserParent>({
	user: Number,
	parentEmail: String,
	parentCode: {
		type: String,
		default: randomString(32),
	},
	reminderTimestamp: {
		type: Number,
		// 24 hours later
		default: () => Date.now() + 1000 * 60 * 60 * 24,
	},
	reminderSent: {
		type: Boolean,
		default: false,
	},
});

export const UserParent = model<IUserParent>("UserParent", UserParentSchema);
