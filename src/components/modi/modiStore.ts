import { get, writable } from "svelte/store";
import { alert } from "~/libgalaxy";

// I'm using snake_case for this in particular
// as a simple way to separate modi names and variables
// So if you have aBcDe you can figure out easier if
// it's "aBc De" or "a BcDe"
// ^ this is actually why tweaks should be inside an object
// * tweaks: global
export const tweaks_hideHeader = writable(false);
export const tweaks_dontLoad = writable(false);
export const tweaks_unloadOnOriginal = writable(false);
export const tweaks_autoExpand = writable(false);
// * tweaks: game
export const tweaks_preventThrottling = writable(false);
export const tweaks_zoom = writable(1);
export const tweaks_brightness = writable(1);
export const tweaks_contrast = writable(100);
export const tweaks_hueRotate = writable(0);
export const tweaks_saturate = writable(100);
export const tweaks_sepia = writable(0);
// * chat
export const chat_generalPopupShown = writable(false);

let storedGame = -1;

function updateTweaksStore() {
	localStorage.setItem(
		"galaxy-tweaks",
		JSON.stringify({
			hideHeader: get(tweaks_hideHeader),
			dontLoad: get(tweaks_dontLoad),
			unloadOnOriginal: get(tweaks_unloadOnOriginal),
			autoExpand: get(tweaks_autoExpand),
		})
	);
	if (storedGame === -1)
		return alert.error("Error saving tweaks. Please report this error!");
	localStorage.setItem(
		`galaxy-tweaks-${storedGame}`,
		JSON.stringify({
			preventThrottling: get(tweaks_preventThrottling),
			zoom: get(tweaks_zoom),
			brightness: get(tweaks_brightness),
			contrast: get(tweaks_contrast),
			hueRotate: get(tweaks_hueRotate),
			saturate: get(tweaks_saturate),
			sepia: get(tweaks_sepia),
		})
	);
}

function uTSIgnoreFirst() {
	let runCount = 0;
	return () => {
		if (runCount >= 1) updateTweaksStore();
		else runCount++;
	};
}

export function loadTweaks(game: number) {
	storedGame = game;
	loadGlobalTweaks();
	loadGameTweaks(game);

	tweaks_hideHeader.subscribe(uTSIgnoreFirst());
	tweaks_dontLoad.subscribe(uTSIgnoreFirst());
	tweaks_unloadOnOriginal.subscribe(uTSIgnoreFirst());
	tweaks_autoExpand.subscribe(uTSIgnoreFirst());

	tweaks_preventThrottling.subscribe(uTSIgnoreFirst());
	tweaks_zoom.subscribe(uTSIgnoreFirst());
	tweaks_brightness.subscribe(uTSIgnoreFirst());
	tweaks_contrast.subscribe(uTSIgnoreFirst());
	tweaks_hueRotate.subscribe(uTSIgnoreFirst());
	tweaks_saturate.subscribe(uTSIgnoreFirst());
	tweaks_sepia.subscribe(uTSIgnoreFirst());
}

function loadGlobalTweaks() {
	const attemptedLoad = localStorage.getItem("galaxy-tweaks");
	if (attemptedLoad === null) return;
	const data = JSON.parse(attemptedLoad);
	tweaks_hideHeader.set(data.hideHeader);
	tweaks_dontLoad.set(data.dontLoad);
	tweaks_unloadOnOriginal.set(data.unloadOnOriginal);
	tweaks_autoExpand.set(data.autoExpand);
}

function loadGameTweaks(game: number) {
	const attemptedLoad = localStorage.getItem(`galaxy-tweaks-${game}`);
	if (attemptedLoad === null) return;
	const data = JSON.parse(attemptedLoad);
	tweaks_preventThrottling.set(data.preventThrottling);
	tweaks_zoom.set(data.zoom);
	tweaks_brightness.set(data.brightness);
	tweaks_contrast.set(data.contrast);
	tweaks_hueRotate.set(data.hueRotate);
	tweaks_saturate.set(data.saturate);
	tweaks_sepia.set(data.sepia);
	console.log(get(tweaks_zoom));
}
